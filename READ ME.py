# The list of user :
# ===================
#     1)username :admin
#       password :admin@123

#     2)username :yasin
#       password :yasin@123

#     3)username :sreeraj
#       password :sreeraj@123



# You can add users using the link below:
# ======================================
# url : host_id/authentication/registration/
# Note : Provide username and password as formdata. It is a post request.



# You can sign in using the link below:
# ======================================
# url : host_id/authentication/login/
# Note : Provide username and password as formdata. It is a post request.



# You can add objects using the link below:
# =========================================
# url:host_id/order/create/
# Note :Provide title and body as formdata.It is a post request. also it need aa access_token.


# You can view objects using the link below:
# =========================================
# url:host_id/order/list/
# Note :It is a get request. It need aa access_token.
