from django.shortcuts import render
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.permissions import AllowAny, IsAuthenticated ,IsAdminUser
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from . serializers import UserRegistrationSerializer, LoginSerializer
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate,login
import requests


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def user_registration(request):
    serialized = UserRegistrationSerializer(data=request.data)
    if serialized.is_valid():
        username = serialized.validated_data['username']
        password = serialized.validated_data['password']
        if User.objects.filter(username=username).exists():
            response_data = {
                "StatusCode": 6001,
                "message": "This username already exists. Please select another username"
            }

        else:
            serialized.save(password=make_password(password))
            response_data = {
                "StatusCode": 6000,
                "data": serialized.data
            }
    else:
        response_data = {
            "StatusCode": 6001,
            "message": serialized._errors
        }
    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def user_login(request):
    serialized = LoginSerializer(data=request.data)
    if serialized.is_valid():
        username = serialized.data['username']
        password = serialized.data['password']
        #check is user is authenticated
        user = authenticate(username=username, password=password)
        if not user:
            response_data = {
                "StatusCode": 6001,
                "message": "User does not exists."
            }
        else:
            customer = User.objects.filter(
                username=username, password=user.password)
            if customer.exists():
                login(request, user)
                headers = {
                    'Content-Type': 'application/json',
                }

                data = '{"username": "' + username + \
                    '", "password":"' + password + '"}'

                protocol = "http://"
                if request.is_secure():
                    protocol = "https://"

                web_host = request.get_host()
                request_url = protocol + web_host + "/authentication/token/"
                response = requests.post(
                    request_url, headers=headers, data=data)
                # check the user role
                if response.status_code == 200:

                    response_data = {
                        "StatusCode": 6000,
                        "data": response.json(),
                        "id" : user.id,
                        "message": "Verified successfully",
                    }
                    return Response(response_data, status=status.HTTP_200_OK)
                else:
                    response_data = {
                        "StatusCode": 6001,
                        "data": serialized.data,
                        "message": "Some error occured please contact Consultant admin to solve this problem."
                    }
                    return Response(response_data, status=status.HTTP_200_OK)
            else:

                response_data = {
                    "StatusCode": 6001,
                    "message": "User does not exists"
                }
                return Response(response_data, status=status.HTTP_200_OK)
    else:
        response_data = {
            "StatusCode": 6001,
            "message":  serialized._errors
        }
    return Response(response_data, status=status.HTTP_200_OK)


