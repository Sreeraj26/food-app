from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Order(models.Model):
    userId = models.ForeignKey(User,on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    body = models.TextField()

    def __str__(self):
        return self.title