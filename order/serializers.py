from rest_framework import serializers
from order.models import Order

class OrderSerializer(serializers.Serializer):
    
    title = serializers.CharField(max_length=100)
    body = serializers.CharField(max_length=1000)

class OrderViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"