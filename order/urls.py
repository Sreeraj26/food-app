from django.urls import path,re_path
from order import views

urlpatterns = [
    path('list/',views.orderview,name='view'),
    path('create/',views.ordercreate,name='view'),
]