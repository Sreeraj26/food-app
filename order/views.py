from django.shortcuts import render,get_object_or_404
from order.models import Order
from order.serializers import OrderSerializer,OrderViewSerializer
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAuthenticated,IsAdminUser
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.renderers import JSONRenderer

# Create your views here.
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def ordercreate(request):
    serialized = OrderSerializer(data=request.data,context={'request':request})
    if serialized.is_valid():
        userId = request.user
        title = serialized.data['title']
        body = serialized.data['body']
        order = Order.objects.create(userId=userId,title=title,body=body)
        response_data = {
            'Statuscode':6000,
            'message' : 'Order created successfully' 
        }
        return Response (response_data,status=status.HTTP_200_OK)
    else :
        response_data={
            "Statuscode":6001,
            "data":serialized.errors
        }
        return Response (response_data,status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def orderview(request):
    instance = Order.objects.order_by("userId","id")
    serialized = OrderViewSerializer(instance,many=True,context={'request':request})
    response_data = {
        "StatusCode ": 6000,
        "data" : serialized.data
    }
    return Response(response_data,status=status.HTTP_200_OK)